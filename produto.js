function Produto(nome, preco){
    this.nome = nome;
    this.preco = preco;
    this.show = function(){
        console.log('Descrição: '+this.nome);
        console.log('Preço: R$ '+this.preco.toFixed(2));
    }
}

var p1 = new Produto('Livro', 50);
var p2 = new Produto('Mesa', 200);
p1.show();
p2.show();