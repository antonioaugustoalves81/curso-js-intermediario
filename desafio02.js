class Rect{
    constructor(base, altura){
        this.base = base;
        this.altura = altura;
    }

    calcularArea(){
        if(this.base !== this.altura){
            console.log('Base: '+this.base);
            console.log('Altura: '+this.altura);
            console.log(`A área do retângulo é igual a ${this.base*this.altura}`)
            console.log('.........................');
        }else{
            console.log('Tamanho do lado: '+this.base);
            console.log(`A área do quadrado é igual a ${this.base*this.altura}`);
            console.log('.........................');

        }
    }
}

class Square extends Rect{
    constructor(lado){
        super(lado, lado);
    }
}

f1 = new Rect(3,6);
f1.calcularArea();

f2 = new Square(5);
f2.calcularArea();