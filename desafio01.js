function Rect(b,h){
    this.base = b;
    this.height = h;

    this.calcArea = function(){
        if (this.base !== this.height){
            return `A área do retângulo é ${this.base*this.height}`;
        }else{
            return `A área do quadrado é ${this.base*this.height}`;
        }
    }
}

function Square(side){
    this.base = side;
    this.height = side;
}

Square.prototype = new Rect();

r1 = new Rect(2, 9);
q1 = new Square(4);

console.log(r1.calcArea());
console.log(q1.calcArea());