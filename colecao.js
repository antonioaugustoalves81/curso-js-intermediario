var obj = new Map();//coleções em js
var a = {'valor':5};
var b = {'valor':10};

obj.set(a, 15);
obj.set(b, 30);
console.log(obj.get(a)+' - '+obj.get(b));

var obj2 = new WeakMap();
var n1 = {'nota':7};
var n2 = {'nota':8};
var n3 = {'nota':6};
obj2.set(n1, 9);
obj2.set(n2, 6);
obj2.set(n3, 8);
console.log(obj2.get(n1)+' - '+obj2.get(n2)+' - '+obj2.get(n3));

//Set é uma coleção que ignora valores repetidos
var conjunto = new Set();
var x = {'valor':3};
var y = {'valor':5};
conjunto.add(x);
conjunto.add(y);
//outros métodos: .has(item), .delete(item), .clear(), .size
