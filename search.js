//Looping em expressões regulares
var regex = /a.c/g;
var str = '123abc a5c567';
console.log(regex.exec(str));
var match;
while(match = regex.exec(str)){
    console.log(`${match[0]} - ${match.index}`);
}