function calcDobro(num){
    if(typeof num !=='number'){
        throw new Error('Você deveria digitar um numero');
    }
    return num *2;
}

try{
    console.log('Dobro: '+calcDobro('4'));
}catch(erro){
        console.log(erro);
        console.log('Dobro: '+calcDobro(4));
    }
