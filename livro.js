function Livro(titulo, autor, paginas){
    //variavel, função  ou atributo privado
    var code = 167;
    //verifica se a variavel é uma instancia de livro
    if(!(this instanceof Livro)){
        return new Livro(titulo, autor, paginas);
    }
    this.titulo = titulo;
    this.autor = autor;
    this.paginas = paginas;
    this.showDetails = function(){
        console.log('Titulo: '+this.titulo);
        console.log('Autor: '+this.autor);
        console.log('Paginas: '+this.paginas);

    }

    this.setCode = function(cod){
        code = cod;
    }

    this.getCode = function(){
        return code;
    }
}

var p1 = new Livro('aaaa', 'bbbb', 50);
var p2 = Livro('bbb', 'cde', 30);

p1.showDetails();
console.log('----------')
p2.showDetails();