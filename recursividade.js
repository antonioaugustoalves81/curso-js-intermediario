//recursividade é quando uma função chama ela mesma
function fatorial(num){
    if(num === 0){
        return 1;
    }else{
        return num * fatorial(num-1);
    }
}

console.log('Fatorial:'+fatorial(6));