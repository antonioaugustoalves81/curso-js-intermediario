function formatDate(aData){
    return aData.replace(/(\d{2})\/(\d{2})\/(\d{4})/,'$3-$2-$1');
}

console.log(formatDate('23/12/1981'));