function Animal(){
    this.sleep = function(){
        console.log('ZZZZZZZ...ZZZZZ...');
    }

    this.walk = function(){
        console.log('I am moving fast...');
    }
}

function Dog(){
    this.bark = function(){
        console.log('Auf Auf Auuuuu!');
    }
}

Dog.prototype = new Animal();//Herança em javascript

var dog = new Dog();