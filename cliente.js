class Pessoa{
    constructor(cpf,cidade){
        this.cpf = cpf;
        this.cidade = cidade;
    }

    showData(){
        console.log('CPF: '+ this.cpf);
        console.log('Naturalidade: '+this.cidade);
    }

    static calcular(){
        console.log(`total: ${1+3}`);
    }
}

class Cliente extends Pessoa{
    constructor(nome, idade){
        this.nome = nome;
        this.idade = idade;
    }

    showMessage(msg){
        console.log(msg);
    }
}